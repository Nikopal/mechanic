import Vue from 'vue'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import './css/mdi.css'
import './css/animate.css'
import './css/style.css'
import 'vuetify/dist/vuetify.min.css'
import Vuetify from "vuetify"
import Vuelidate from 'vuelidate'
import "./rivg/velocity"
import store from './store'
import ajaxpost from "./ajaxpost.js"
import router from './router'
import VueQRCodeComponent from 'vue-qrcode-component'
import vTextGroup from "@/comp/vTextGroup"

Vue.config.productionTip = false
Vue.component('qr-code', VueQRCodeComponent)
Vue.component('v-text-group', vTextGroup)
Vue.use(Vuetify)
Vue.use(Vuelidate)
Vue.use(ajaxpost)

const vuetify = new Vuetify({
    lang: {
        current: 'ru',
    },
    icons: {
        iconfont: "mdi"
    },
});

window.myvue = new Vue({
    vuetify,
    router,
    store,
    render: h => h(App)
}).$mount('#app')
