 parseJSON = function (json) {//Обработка JSON строки
        let parsed;
        try {
            parsed = JSON.parse(json)
        } catch (e) {
            parsed = null;
        }
        return parsed;
    }

function getXmlHttp() {
    let xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function getDataX(url, posdData, funexcec) {
    var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xmlhttp = new XHR();
    xmlhttp.open('post', url, true);
    xmlhttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    try {
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4) {
                if (xmlhttp.status === 0) {
                    if (funexcec !== undefined) {
                        //funexcec('[{"error":"Unknown Error Occured. Server response not received."}]');
                    }
                    return false;
                }
                if (xmlhttp.status === 200) {
                    if (funexcec !== undefined) {
                        funexcec(xmlhttp.responseText);
                    }
                }
            }
        };
        xmlhttp.send(posdData);
    } catch (e) {
        //console.log('catch', e);
    }
};
