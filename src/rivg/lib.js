export function IsJsonString(str) { //Првоерка текста на JSON
    if (str != null) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    } else
        return false;
}

export function prtlGetNow() {
    let date = new Date();

    let dd = date.getDate();
    if (dd < 10)
        dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10)
        mm = '0' + mm;

    let yy = date.getFullYear();
    if (yy < 10)
        yy = '0' + yy;

    let HH = date.getHours() % 100;
    if (HH < 10)
        HH = '0' + HH;

    let MM = date.getMinutes() % 100;
    if (MM < 10)
        MM = '0' + MM;

    let SS = date.getSeconds() % 100;
    if (SS < 10)
        SS = '0' + SS;

    return  dd  + '-' + mm + '-' + yy + ' ' + HH + ':' + MM + ':' + SS;
}

export function dateReal() {
    let date = new Date();

    let dd = date.getDate();
    if (dd < 10)
        dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10)
        mm = '0' + mm;

    let yy = date.getFullYear();
    if (yy < 10)
        yy = '0' + yy;

    let HH = date.getHours() % 100;
    if (HH < 10)
        HH = '0' + HH;

    let MM = date.getMinutes() % 100;
    if (MM < 10)
        MM = '0' + MM;

    let SS = date.getSeconds() % 100;
    if (SS < 10)
        SS = '0' + SS;

    return dd + '.' + mm + '.' + yy;
}

export function timetOsKREAN() {
    let date = new Date();

    let dd = date.getDate();
    if (dd < 10)
        dd = '0' + dd;

    let mm = date.getMonth() + 1;
    if (mm < 10)
        mm = '0' + mm;

    let yy = date.getFullYear();
    if (yy < 10)
        yy = '0' + yy;

    let HH = date.getHours() % 100;
    if (HH < 10)
        HH = '0' + HH;

    let MM = date.getMinutes() % 100;
    if (MM < 10)
        MM = '0' + MM;

    let SS = date.getSeconds() % 100;
    if (SS < 10)
        SS = '0' + SS;

    return HH + ':' + MM;
}

window.parseJSON = function (json) {//Обработка JSON строки
    let parsed;
    try {
        parsed = JSON.parse(json)
    } catch (e) {
        parsed = null;
    }
    return parsed;
}

window.apromGPS = function (x, y) {//Формируем массив
    gpsMass.pop();
    let koodLL = {};
    koodLL.lat = x;
    koodLL.lon = y;
    gpsMass.unshift(koodLL);
    calcDistans();
}

window.getXYgps = function () {
    let maxVal = 0;
    let JmaxVal = {};
    JmaxVal.lan = 0;
    JmaxVal.lon = 0;
    gpsMassM.forEach(function (item, i, gpsMassM) {
        console.log(i, gpsMassM[i]);
        if (i == 0) {
            maxVal = item.dist;
            JmaxVal = item;
        } else {
            if (maxVal > item.dist) {
                maxVal = item.dist;
                JmaxVal = item;
            }
        }
    });
    return JmaxVal;
}
