const replaceLat2Rus = str => str
    .replace(/A/gmi, 'А')
    .replace(/B/gmi, 'В')
    .replace(/E/gmi, 'Е')
    .replace(/K/gmi, 'К')
    .replace(/M/gmi, 'М')
    .replace(/H/gmi, 'Н')
    .replace(/O/gmi, 'О')
    .replace(/P/gmi, 'Р')
    .replace(/C/gmi, 'С')
    .replace(/T/gmi, 'Т')
    .replace(/X/gmi, 'Х')
    .replace(/Y/gmi, 'У')
;
const regex = () => ([
    /^([АВЕКМНОРСТХУ]\s*\d{3}\s*[АВЕКМНОРСТХУ]{2}\s*\d{2,3})$/mi,
    /^((Т\s*)?[АВЕКМНОРСТХУ]{1,2}\s*\d{3,4}\s*\d{2,3})$/mi,
    /^(\d{3,4}\s*[АВЕКМНОРСТХУ]{2}\s*\d{2,3})$/mi,
    /^(\d{3,4}\s*[АВЕКМНОРСТХУ]{1,2}\s*\d{0,3}\s*\d{2,3})$/mi,
    /^(\d{3,4}\s*[АВЕКМНОРСТХУD]{1,2}\s*\d{0,3}\s*\d{2,3})$/mi,
    /^([АВЕКМНОРСТХУ]{2}\s*\d{3}\s*[АВЕКМНОРСТХУ]\s*\d{2,3})$/mi,
    /^((?:['"]?ТРАНЗИТ['"]?)?\s*[АВЕКМНОРСТХУ]{2}\s*\d{2,3}\s*(?:['"]?ТРАНЗИТ['"]?)?\s*\d{3,4}\s*(?:['"]?ТРАНЗИТ['"]?)?)$/mi
]);

export const IsAutoNumber = str => {
    str = replaceLat2Rus((str || "").trim().toUpperCase());
    let r = regex();
    for (let i = 0, maxI = r.length; i < maxI; i++) {
        if (r[i].test(str)) {
            return true;
        }
    }
    return false;
}
export const GetAutoNumber = str => {
    str = replaceLat2Rus((str || "").trim().toUpperCase());
    let r = regex();
    for (let i = 0, maxI = r.length; i < maxI; i++) {
        let m = r[i].exec(str);
        if (m) {
            return m[1].replace(/ /gm, '');
        }
    }
    return false;
}