import dlgAddAuto from "@/comp/AutoList/dlgAddAuto/dlgAddAuto";
import dlgStatusAuto from "@/comp/AutoList/dlgStatusAuto/dlgStatusAuto";
import reference from "@/comp/AutoList/reference/reference";
import AutoListItem from "@/comp/AutoList/AutoListItem/AutoListItem";
import {mapActions} from "vuex"

export default {
    name: 'Autolist',

    components: {dlgAddAuto, dlgStatusAuto, reference, AutoListItem},

    data() {
        return {
            search: '',
            statusAuto: false,
            addAuto: false,
            referenceDlg: false,
            list: [],
            st: {},
        }
    },
    mounted() {
        if (localStorage.getItem('autoList') === null) {
            this.getAuto();
        } else {
            this.list = JSON.parse(localStorage.getItem('autoList'))
        }
    },
    computed: {
        AutoListComput() {
            return this.list.filter(item => item.NameAvtoW.toLowerCase().indexOf(this.search.toLowerCase()) !== -1)
        }
    },
    methods: {
        ...mapActions({
            overlay: 'actOverlay',
            snackbar: 'act_snackbar'
        }),
        opnStatus(st) {
            this.statusAuto = true;
            this.st = st;
        },
        setStatus(e) {
            let ind = this.list.findIndex(item => item.idW === e.id);
            this.list[ind].typePosition = e.item.val;
            let arr = this.list[ind]
            this.list.splice(ind, 1)
            this.list.push(arr)
        },
        async getAuto() {
            this.list = await this.$post(this.$store.getters.getApi.urlApi, {//Пинг отправляем
                rout: 'mobileAPIClass',
                func: 'mobileApiFnc',
                INN: this.$store.getters.getUser.info.INN,
                KPP: this.$store.getters.getUser.info.KPP,
            })
            if(this.list.length === 0){
                this.snackbar({flag:true, text:'Авто не найдено', color:'warning'})
            }else {
                localStorage.setItem('autoList', JSON.stringify(this.list))
            }
        },

    }
}
